# Projeto Default | Nunjucks

Clone o projeto com sua chave SSH e execute os seguintes comandos:

- git checkout trunk
- npm install
- gulp

### **Agora é só trabalhar!! :)**

---
## **REGISTRAR NOVO COMPONENTE**

```
./src/components/_macros/
  |__ novo-componente
      |__ macro.njk (HTML em formato Nunjucks)
      |__ style.scss (CSS em formato SCSS)
      |__ script.js (JS em formato jQuery)
      |__ readme.md (Documentação do componente)


./src/components/base.njk
  <!-- Importar componente dentro do bloco de macros -->

  {% block macros %}
    {% from "nome-componente/macro.njk" import nomeComponente %}
  {% endblock %}
```

> Pare de executar o Gulp **(CTRL + C)** e execute novamente para que o Gulp passe a reconhecer o novo componente criado.


---
## **ADICIONAR NOVA IMAGEM**
> As novas imagens deve ser adicionas na pasta **./src/images**.

> Após a inclusão, pare de executar o Gulp **(CTRL + C)** e execute novamente para que o Gulp passe a reconhecer as novas imagens.


---
## **ADICIONAR NOVA PÁGINA**

```
./src/views/
    |__ nome-pagina.njk (HTML em formato Nunjucks)
```

> As novas páginas devem ser criadas na pasta **src/views**.

> Após a inclusão, pare de executar o Gulp **(CTRL + C)** e execute novamente para que o Gulp passe a reconhecer as novas páginas.

> Segue abaixo modelo de inicialização de página

```
{# -------------- #}
{% set pagename = "Home" %}
{# -------------- #}

{% extends "default.njk" %}

{% block header %}

{% endblock %}

{% block content %}

{{
  title(
    config = {
      uppercase: true
    },
    data = {
      title: 'Title'
    }
  )
}}

{% endblock %}
```

---
## Adicionar ícone do Font Awesome
Utilize o link a seguir para adicionar o **CDN** no projeto e utilizar os ícones da biblioteca.

[Start Font Awesome](https://fontawesome.com/start)


---
## Dependências JS
Adicione as dependências utilizando **NPM**

- [jquery-autocomplete](https://www.npmjs.com/package/jquery-autocomplete)
- [jquery-mask-plugin](https://www.npmjs.com/package/jquery-mask-plugin)
- [swiper](https://www.npmjs.com/package/swiper)

> Após a instalção o arquivo js minificado da dependência deve ser chamado no arquivo **./gulp.paths.json***

> A seguir veja o exemplo de como o código deve ficar.

```
"js": {
  "dependencies": [
    "./node_modules/jquery/dist/jquery.min.js",
    "ADICIONAR CAMINHO DO ARQUIVO AQUI"
  ],
},
```

> Após a inclusão, pare de executar o Gulp **(CTRL + C)** e execute novamente para que o Gulp passe a reconhecer as novas dependências.

---
---
---

Não se esqueça de criar seu repositório no git.

Até mais!

## [Victor Marques](http://victormarques.me/)
